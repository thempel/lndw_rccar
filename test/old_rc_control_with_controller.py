import serial
import pygame
from sys import exit
from pygame.locals import *


class RCTest(object):

    def __init__(self):
        self.ser = serial.Serial('/dev/ttyACM0', 115200, timeout=1)
        self.send_inst = True
        pygame.init()
        self.screen=pygame.display.set_mode((640,480),0,24)
        pygame.display.set_caption("Key Press Test")
        self.joystick = pygame.joystick.Joystick(0);
        self.joystick.init()
        self.steer()

    def steer(self):

        print("Steering")
        while self.send_inst:
            for event in pygame.event.get():
                print("event")
                if event.type == KEYDOWN:
                    key_input = pygame.key.get_pressed()

                    # complex orders
                    if key_input[pygame.K_UP] and key_input[pygame.K_RIGHT]:
                        print("Forward Right")
                        self.ser.write(chr(6))

                    elif key_input[pygame.K_UP] and key_input[pygame.K_LEFT]:
                        print("Forward Left")
                        self.ser.write(chr(7))

                    elif key_input[pygame.K_DOWN] and key_input[pygame.K_RIGHT]:
                        print("Reverse Right")
                        self.ser.write(chr(8))

                    elif key_input[pygame.K_DOWN] and key_input[pygame.K_LEFT]:
                        print("Reverse Left")
                        self.ser.write(chr(9))

                    # simple orders
                    elif key_input[pygame.K_UP]:
                        print("Forward")
                        self.ser.write(chr(1))

                    elif key_input[pygame.K_DOWN]:
                        print("Reverse")
                        self.ser.write(chr(2))

                    elif key_input[pygame.K_RIGHT]:
                        print("Right")
                        self.ser.write(chr(3))

                    elif key_input[pygame.K_LEFT]:
                        print("Left")
                        self.ser.write(chr(4))

                    # exit
                    elif key_input[pygame.K_x] or key_input[pygame.K_q]:
                        print ("Exit")
                        self.send_inst = False
                        self.ser.write(chr(0))
                        self.ser.close()
                        break

                elif event.type == pygame.KEYUP or event.type == pygame.JOYBUTTONUP:
                    print ("Noting")
                    self.ser.write(chr(0))
                elif event.type == pygame.JOYBUTTONDOWN:
                    key_input = [];
                    for i in range(self.joystick.get_numbuttons() ):
                        key_input.append(self.joystick.get_button( i ));

                    # complex orders
                    if key_input[4] and key_input[5]:
                        print("Forward Right")
                        self.ser.write(chr(6))

                    elif key_input[4] and key_input[7]:
                        print("Forward Left")
                        self.ser.write(chr(7))

                    elif key_input[6] and key_input[5]:
                        print("Reverse Right")
                        self.ser.write(chr(8))

                    elif key_input[6] and key_input[7]:
                        print("Reverse Left")
                        self.ser.write(chr(9))

                    # simple orders
                    elif key_input[4]:
                        print("Forward")
                        self.ser.write(chr(1))

                    elif key_input[6]:
                        print("Reverse")
                        self.ser.write(chr(2))

                    elif key_input[5]:
                        print("Right")
                        self.ser.write(chr(3))

                    elif key_input[7]:
                        print("Left")
                        self.ser.write(chr(4))


if __name__ == '__main__':
    RCTest()
