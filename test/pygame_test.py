import pygame
from pygame.locals import *

class PygameTest(object):

    def __init__(self):

        # initialise pygame
        pygame.init()
        # apparently one needs to create a display to make pygame work
        self.screen=pygame.display.set_mode((320,240))
        #self.screen=pygame.display.set_mode((640,480),0,24)
        self.steer()

    def steer(self):

        stop = False
        # stream video frames one by one
        while not stop:
            for event in pygame.event.get():
                if event.type == KEYDOWN or event.type==KEYUP:
                    key = pygame.key.get_pressed()

                    if key[pygame.K_q]:
                        print 'Exit'
                        stop = True
                        break

                    control = [key[K_UP], key[K_DOWN], key[K_LEFT], key[K_RIGHT]]
                    print 'Control', ''.join(str(e) for e in control)

if __name__ == '__main__':
    PygameTest()
