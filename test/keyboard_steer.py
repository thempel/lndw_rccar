import numpy as np
import cv2
import pygame
from pygame.locals import *
import socket

class SteerWithScreenTest(object):

    def __init__(self):

        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)
        self.server_socket.bind(('192.168.2.101', 8010)) #local network for university
        #self.server_socket.bind(('192.168.178.79', 8010)) #jonnys home network
        self.server_socket.listen(0)

        print 'Wait for webcam'
        self.web_connection = self.server_socket.accept()[0].makefile('rb')

        print 'Wait for control'
        self.steer_connection = self.server_socket.accept()[0]

        # connect to a seral port
        self.send_inst = True

        # initialise pygame
        pygame.init()
        # apparently one needs to create a display to make pygame work
        #self.screen=pygame.display.set_mode((320,240))
        self.screen=pygame.display.set_mode((640, 480))
        self.steer()

    def steer(self):

        # collect images for training
        print 'Start steering...'

        # stream video frames one by one
        try:
            stream_bytes = ' '
            frame = 1
            while self.send_inst:
                stream_bytes += self.web_connection.read(1024)
                first = stream_bytes.find('\xff\xd8')
                last = stream_bytes.find('\xff\xd9')
                if first != -1 and last != -1:
                    jpg = stream_bytes[first:last + 2]
                    stream_bytes = stream_bytes[last + 2:]
                    image = cv2.imdecode(np.fromstring(jpg, dtype=np.uint8), cv2.CV_LOAD_IMAGE_GRAYSCALE)

                    # cv2 pictures are stored in BGR not RGB so we have to fix color before displaying it
                    frame=cv2.cvtColor(image,cv2.COLOR_GRAY2RGB)
                    # rotate
                    frame=np.rot90(frame,3)
                    frame=np.fliplr(frame)
                    # resize picture to bigger screen
                    frame = cv2.resize(frame,(480,640))
                    #create surface from picture
                    img=pygame.surfarray.make_surface(frame)
                    self.screen.blit(img,(0,0))
                    pygame.display.flip() #update display

                    # get input from human driver
                    for event in pygame.event.get():
                        if event.type == KEYDOWN or event.type == KEYUP:
                            key = pygame.key.get_pressed()

                            # quit if q is pressed on the keyboard
                            if key[K_q]:
                                print 'Exit'
                                self.send_inst = False
                                self.steer_connection.sendall('q')
                                break
                            if key[K_PLUS]:
                                self.steer_connection.sendall('+')
                                print 'Speed-Up'
                            elif key[K_MINUS]:
                                print 'Slowdown'
                                self.steer_connection.sendall('-')
                            else:
                                control = [key[K_UP], key[K_DOWN], key[K_LEFT], key[K_RIGHT]]
                                print 'Control',control
                                self.steer_connection.sendall(''.join(str(e) for e in control))
        finally:
            self.web_connection.close()
            self.steer_connection.close()
            self.server_socket.close()

if __name__ == '__main__':
    SteerWithScreenTest()
