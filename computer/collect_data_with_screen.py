import numpy as np
import cv2
import pygame
from pygame.locals import *
import socket


class CollectTrainingData(object):

    def __init__(self):

        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)
        self.server_socket.bind(('192.168.2.101', 8010)) #local network for university 
        #self.server_socket.bind(('192.168.178.21', 8010)) #jonnys home network
        self.server_socket.listen(0)

        print 'Wait for webcam'
        self.web_connection = self.server_socket.accept()[0].makefile('rb')

        print 'Wait for control'
        self.steer_connection = self.server_socket.accept()[0]

        self.send_inst = True

        # create labels
        self.k = np.zeros((4, 4), 'float')
        for i in range(4):
            self.k[i, i] = 1

        pygame.init()
        self.screen=pygame.display.set_mode((320,240))#,0,24)
        self.collect_image()

    def collect_image(self):

        saved_frame = 0
        total_frame = 0

        # collect images for training
        print 'Start collecting images...'
        e1 = cv2.getTickCount()
        image_array = np.zeros((1, 38400))
        label_array = np.zeros((1, 4), 'float')

        # stream video frames one by one
        try:
            stream_bytes = ' '
            frame = 1
            while self.send_inst:
                stream_bytes += self.web_connection.read(1024)
                first = stream_bytes.find('\xff\xd8')
                last = stream_bytes.find('\xff\xd9')
                if first != -1 and last != -1:
                    jpg = stream_bytes[first:last + 2]
                    stream_bytes = stream_bytes[last + 2:]
                    image = cv2.imdecode(np.fromstring(jpg, dtype=np.uint8), cv2.CV_LOAD_IMAGE_GRAYSCALE)

                    # select lower half of the image
                    roi = image[120:240, :]

                    # save streamed images
                    cv2.imwrite('training_images/frame{:>05}.jpg'.format(frame), image)

                    # show picture with cv2
                    #cv2.imshow('roi_image', roi)

                    # load picture as pygame.surface, might be slow
                    #img=pygame.image.load('training_images/frame{:>05}.jpg'.format(frame)) #load image from desk

                    # cv2 pictures are stored in BGR not RGB so we have to fix color before displaying it
                    frame2=cv2.cvtColor(image,cv2.COLOR_GRAY2RGB)
                    # rotate
                    #frame2=np.rot90(frame2)

                    frame2=np.rot90(frame2,3)
                    frame2=np.fliplr(frame2)
                    #create surface from picture
                    img=pygame.surfarray.make_surface(frame2)
                    self.screen.blit(img,(0,0))
                    pygame.display.flip() #update display

                    # reshape the roi image into one row array
                    temp_array = roi.reshape(1, 38400).astype(np.float32)

                    frame += 1
                    total_frame += 1

                    # get input from human driver
                    for event in pygame.event.get():
                        if event.type == KEYDOWN or event.type == KEYUP:
                            key = pygame.key.get_pressed()

                            # quit if q is pressed on the keyboard
                            if key[K_q]:
                                print 'Exit'
                                self.send_inst = False
                                self.steer_connection.sendall('q')
                                break
                            if key[K_PLUS]:
                               self.steer_connection.sendall('+')
                               print 'Speed-Up'
                            elif key[K_MINUS]:
                               print 'Slowdown'
                               self.steer_connection.sendall('-')


                            # parse control
                            control = [key[K_UP], key[K_DOWN], key[K_LEFT], key[K_RIGHT]]
                            print 'Control ',control

                            # in the original software, the movements back+right and back+left are ignored...should we add it?
                            # complex orders
                            if key[K_UP]:
                                k = 2
                                if key[K_RIGHT]:
                                    k = 1
                                elif key[K_LEFT]:
                                    k = 0

                                image_array = np.vstack((image_array, temp_array))
                                label_array = np.vstack((label_array, self.k[k]))
                                saved_frame += 1

                            elif key[pygame.K_DOWN]:

                                saved_frame += 1
                                image_array = np.vstack((image_array, temp_array))
                                label_array = np.vstack((label_array, self.k[3]))

                            elif key[pygame.K_RIGHT]:
                                image_array = np.vstack((image_array, temp_array))
                                label_array = np.vstack((label_array, self.k[1]))
                                saved_frame += 1

                            elif key[pygame.K_LEFT]:
                                image_array = np.vstack((image_array, temp_array))
                                label_array = np.vstack((label_array, self.k[0]))
                                saved_frame += 1

                            # send control to raspberry
                            self.steer_connection.sendall(''.join(str(e) for e in control))

            # save training images and labels
            train = image_array[1:, :]
            train_labels = label_array[1:, :]

            # save training data as a numpy file
            np.savez('training_data/test08.npz', train=train, train_labels=train_labels)

            e2 = cv2.getTickCount()
            # calculate streaming duration
            time0 = (e2 - e1) / cv2.getTickFrequency()
            print 'Streaming duration:', time0

            print(train.shape)
            print(train_labels.shape)
            print 'Total frame:', total_frame
            print 'Saved frame:', saved_frame
            print 'Dropped frame', total_frame - saved_frame

        finally:
            self.web_connection.close()
            self.steer_connection.close()
            self.server_socket.close()

if __name__ == '__main__':
    CollectTrainingData()
