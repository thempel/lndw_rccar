import threading
import SocketServer
import cv2
import numpy as np
import math
import socket

class NeuralNetwork(object):

    def __init__(self):
        self.model = cv2.ANN_MLP()

    def create(self):
        layer_size = np.int32([38400, 32, 4])
        self.model.create(layer_size)
        self.model.load('mlp_xml/mlp.xml')

    def predict(self, samples):
        ret, resp = self.model.predict(samples)
        return resp.argmax(-1)


class RCControl(object):

    def __init__(self):

        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR,1)
        self.server_socket.bind(('192.168.2.101', 8010)) #local network for university 
        #self.server_socket.bind(('192.168.178.79', 8000)) #jonnys home network
        self.server_socket.listen(0)

        print 'Wait for webcam'
        self.web_connection = self.server_socket.accept()[0].makefile('rb')

        print 'Wait for control'
        self.steer_connection = self.server_socket.accept()[0]

        # create neural network
        self.model = NeuralNetwork()
        self.model.create()
        self.handle()


    def steer(self, prediction):
        if prediction == 2:
            self.steer_connection.sendall('1000')
            print 'Forward'
        elif prediction == 0:
            self.steer_connection.sendall('1010')
            print 'Forward Left'
        elif prediction == 1:
            self.steer_connection.sendall('1001')
            print 'Forward Right'
        else:
            self.steer_connection.sendall('0000')

    def handle(self):

        stream_bytes = ' '

        # stream video frames one by one
        try:
            while True:
                stream_bytes += self.web_connection.read(1024)
                first = stream_bytes.find('\xff\xd8')
                last = stream_bytes.find('\xff\xd9')
                if first != -1 and last != -1:
                    jpg = stream_bytes[first:last+2]
                    stream_bytes = stream_bytes[last+2:]
                    gray = cv2.imdecode(np.fromstring(jpg, dtype=np.uint8), cv2.CV_LOAD_IMAGE_GRAYSCALE)
                    image = cv2.imdecode(np.fromstring(jpg, dtype=np.uint8), cv2.CV_LOAD_IMAGE_UNCHANGED)

                    # lower half of the image
                    half_gray = gray[120:240, :]

                    cv2.imshow('image', image)
                    #cv2.imshow('mlp_image', half_gray)

                    # reshape image
                    image_array = half_gray.reshape(1, 38400).astype(np.float32)

                    # neural network makes prediction
                    prediction = self.model.predict(image_array)

                    self.steer(prediction)

        finally:
            self.web_connection.close()
            self.steer_connection.close()
            self.server_socket.close()
            cv2.destroyAllWindows()

if __name__ == '__main__':
    RCControl()
