import socket
from gpiozero import LED
import RPi.GPIO as GPIO
from time import sleep


class PWMControlTest(object):
    def __init__(self):

         GPIO.setmode(GPIO.BCM)
         GPIO.setup(24, GPIO.OUT)

         F = 20000 #50
         self.led1 = GPIO.PWM(24,F)
         self.led2 = LED(23)
         self.led3 = LED(17)
         self.led4 = LED(27)

         speed = 80
         self.speed = speed
         self.led1.ChangeDutyCycle(speed)

         self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
         self.server_socket.connect(('192.168.2.101', 8010)) # setup for local network at uni
         #self.server_socket.connect(('192.168.178.79', 8010)) # setup for Jonnys home network
         self.streaming()

    def control(self, f, b, l, r):
        if f=='1':
            self.led1.start(self.speed)
        else:
            self.led1.stop()

        if b=='1':
            self.led2.on()
        else:
            self.led2.off()

        if l=='1':
            self.led3.on()
        else:
            self.led3.off()

        if r=='1':
            self.led4.on()
        else:
            self.led4.off()


    def streaming(self):

        try:
            print "Streaming..."
            print "Press 'q' to exit"

            while True:
                data = self.server_socket.recv(4)
#                print 'Data ',data

                if data:
                    if data[0] == 'q':
                        break
                    if data[0] == '+':
                        self.speed = min(100,self.speed+5)
                    elif data[0] == '-':
                        self.speed = max(0,self.speed-5)
                    if len(data)==4:
                        self.control(data[0],data[1],data[2],data[3])
            print 'quit succ'
        finally:
           self.server_socket.close()

if __name__ == '__main__':
    PWMControlTest()
