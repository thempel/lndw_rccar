import socket
from gpiozero import LED
from time import sleep


class VideoStreamingTest(object):
    def __init__(self):

        self.led1 = LED(24) #27
        self.led2 = LED(23) #17
        self.led3 = LED(17) #24
        self.led4 = LED(27) #23

        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.connect(('192.168.2.101', 8010)) # setup for local network at uni
        #self.server_socket.bind(('192.168.178.51', 8010)) # setup for Jonnys home network
        self.streaming()

    def control(self, f, b, l, r):
        if f=='1':
            self.led1.on()
        else:
            self.led1.off()

        if b=='1':
            self.led2.on()
        else:
            self.led2.off()

        if l=='1':
            self.led3.on()
        else:
            self.led3.off()

        if r=='1':
            self.led4.on()
        else:
            self.led4.off()


    def streaming(self):

        try:
            print "Streaming..."
            print "Press 'q' to exit"

            while True:
                data = self.server_socket.recv(4)
#                print 'Data ',data

                if data:
                    if data[0] == 'q':
                        break
                    if len(data)==4:
                        self.control(data[0],data[1],data[2],data[3])
            print 'quit succ'
        finally:
           self.server_socket.close()

if __name__ == '__main__':
  VideoStreamingTest()
