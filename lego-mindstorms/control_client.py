import socket
import ev3dev.ev3 as ev3


class VideoStreamingTest(object):
    def __init__(self):

        self.motor_left = ev3.LargeMotor('outD')
        self.motor_right = ev3.LargeMotor('outA')


        self.server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.server_socket.connect(('192.168.2.101', 8010)) # setup for local network at uni
        #self.server_socket.bind(('192.168.178.51', 8010)) # setup for Jonnys home network
        self.streaming()

    def forward(self):
       self.motor_left.run_direct(duty_cycle_sp=75)
       self.motor_right.run_direct(duty_cycle_sp=75)

    def back(self):
       self.motor_left.run_direct(duty_cycle_sp=-75)
       self.motor_right.run_direct(duty_cycle_sp=-75)

    def forward_left(self):
       self.motor_left.run_direct( duty_cycle_sp=25)
       self.motor_right.run_direct( duty_cycle_sp=100)

    def forward_right(self):
       self.motor_left.run_direct( duty_cycle_sp=100)
       self.motor_right.run_direct( duty_cycle_sp=25)

    def backward_left(self):
       self.motor_left.run_direct( duty_cycle_sp=-25)
       self.motor_right.run_direct( duty_cycle_sp=-100)

    def backward_right(self):
       self.motor_left.run_direct( duty_cycle_sp=-100)
       self.motor_right.run_direct( duty_cycle_sp=-25)

    def stop(self):
       self.motor_left.run_direct( duty_cycle_sp=0)
       self.motor_right.run_direct( duty_cycle_sp=-0)


    def streaming(self):

        try:
            print "Streaming..."
            print "Press 'q' to exit"

            while True:
                data = self.server_socket.recv(4)
#                print 'Data ',data

                if data:
                    if data[0] and not (data[1] or data[2] or data[3]):
                        self.forward()
                    elif data[1] and not (data[0] or data[2] or data[3]):
                        self.back()
                    elif (data[0] and data[2]) and not (data[1] or data[3]):
                        self.forward_left()
                    elif (data[0] and data[3]) and not (data[1] or data[2]):
                        self.forward_right()
                    elif (data[1] and data[2]) and not (data[0] or data[3]):
                        self.backward_left()
                    elif (data[1] and data[3]) and not (data[0] or data[2]):
                        self.backward_right()
                    elif data[0] == 'q':
                        break
                    else:
                        self.stop()

        finally:
           self.server_socket.close()

if __name__ == '__main__':
  VideoStreamingTest()
